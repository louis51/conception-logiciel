from fastapi import FastAPI
from pydantic import BaseModel
import requests

app = FastAPI()

var_id = ""


class Data(BaseModel):
    nombre_cartes: int


@app.get("/")
def Root():
    return {"Essai"}


@app.get("/creer-un-deck-melange")
def creerdeckmelange():
    global var_id
    req = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    data = req.json()

    var_id = data["deck_id"]
    return {"deck_id": var_id}


@app.post("/cartes")
def tirerunecarte(da: Data):
    req = requests.get("https://deckofcardsapi.com/api/deck/" + var_id + "/draw/?count=" + str(da.nombre_cartes))
    json = req.json()


    return {"deck_id": var_id, "cards": json["cards"]}


