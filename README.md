PHILBERT Louis
# Rendu de travaux pratique : conception de logiciel 

L'application est composée de deux parties, une partie serveur et une partie client 

Partie serveur :

Installation des dépendances requises

`pip install -r requirements.txt`

Lancement de l'application 

`uvicorn main:app --reload`

Partie client 

Installation des dépendances requises

`pip install -r requirements.txt`

Lancement de l'application

`python main.py`

Lien pour lancer l'application 

http://127.0.0.1:8000/



